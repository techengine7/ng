'use strict';

bookApp.controller('BookListCtrl', function($scope, $http) {

    $http.get('data/books.json').success(function(data) {
        $scope.books = data;
    });

});

bookApp.controller('BookViewCtrl', function($scope, $http) {

    $http.get('data/book.json').success(function(data) {
        $scope.oneBook = data;
    });

});